
const FIRST_NAME = "Popescu";
const LAST_NAME = "Teodor";
const GRUPA = "1092";

class Employee {
    constructor(name, surname, salary)
    {
    	this.name = name;
    	this.surname = surname;
    	this.salary = salary;
    }

getDetails()
 {
 	return this.name.toString()+" "+ this.surname.toString()+" "+ this.salary.toString();
 }
    
}

class SoftwareEngineer extends Employee {

	constructor(name, surname, salary, experience = 'JUNIOR')
	{
		super(name,surname,salary);
        this.experience = experience;
	}

	applyBonus()
	{
		if(this.experience === 'JUNIOR')
			return  this.salary * 1.1;
		else
			if(this.experience === 'MIDDLE')
				return  this.salary * 1.15;
			else
				if(this.experience === 'SENIOR')
					return  this.salary * 1.2;
		
		return this.salary * 1.1;
	}
   
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

